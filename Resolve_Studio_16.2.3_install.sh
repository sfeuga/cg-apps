#!/usr/bin/env bash

if [[ $(grep "Fedora 32" /etc/os-release) ]]; then
  echo "Download Resolve Studio 16.2.3 installer"
  wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Resolve_Studio_16.2.3/DaVinci_Resolve_Studio_16.2.3_Linux.run -O /tmp/DaVinci_Resolve_Studio_16.2.3_Linux.run
  if [[ ! "$?" == 0 ]]; then
    echo "Install failed, nothing to install"
    exit 2
  fi

  (
    if [[ -e "/opt/resolve" ]]; then
      echo "Please, unintall Resolve first"
      sleep 3
      sudo /opt/resolve/installer
      sudo rm -rf /opt/resolve
    fi

    echo "Install Resolve Studio"
    chmod 755 /tmp/DaVinci_Resolve_Studio_16.2.3_Linux.run
    sudo /tmp/DaVinci_Resolve_Studio_16.2.3_Linux.run
  )
  if [[ "$?" != "0" ]]; then
    echo "Install failed"
    exit 2
  fi

  echo "Do you have a license ? (y/N)"
  read yn
  case $yn in
    [yY] | [yY][Ee][Ss] )
      echo "You can now register your license"
      ;;
    [nN] | [n|N][O|o] | * )
      wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Resolve_Studio_16.2.3/Resolve_Studio_16.2.3_Patch.zip
      unzip -o Resolve_Studio_16.2.3_Patch.zip
      if [[ -f "resolve.patch" ]]; then
        cp /opt/resolve/bin/resolve.bin /opt/resolve/bin/resolve.bin.old
        sudo patch < resolve.patch /opt/resolve/bin/resolve.bin
        chmod 775 /opt/resolve/bin/resolve.bin
      fi
      ;;
  esac

  echo "You can now run Resolve Studio \\o/"
fi
