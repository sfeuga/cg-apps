#!/usr/bin/env bash

if [[ $(grep "Fedora 32" /etc/os-release) ]]; then
  echo "Download Clarisse IFX installer"
  wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Clarisse_iFX_v4.0_SP8/isotropix_clarisse_ifx_4.0_sp8_linux64.tar.gz -O /tmp/isotropix_clarisse_ifx_4.0_sp8_linux64.tar.gz
  if [[ ! "$?" == 0 ]]; then
    echo "Install failed, nothing to install"
    exit 2
  fi

  (
    if [[ -e "/opt/isotropix/clarisse" ]]; then
      sudo rm -rf /opt/isotropix/clarisse
    fi
    echo "Install missings components"
    sudo dnf install -y python27.x86_64

    echo "Install Clarisse IFX"
    sudo mkdir -p /opt/isotropix
    sudo chown sfo:root /opt/isotropix
    cd /opt/isotropix
    echo -e "#!/usr/bin/env bash\n/opt/isotropix/clarisse/clarisse" > /opt/isotropix/startup
    chmod 755 /opt/isotropix/startup
    cat <<EOF > /opt/isotropix/"clarisse.desktop"
[Desktop Entry]
Name=Clarisse 4.0 SP8
Type=Application
Categories=Application;Graphics;
Icon=/opt/isotropix/clarisse.png
Exec=/opt/isotropix/startup
Encoding=UTF-8
EOF
    wget https://gitlab.com/sfeuga/cg-apps/-/raw/Clarisse_iFX_v4.0_SP8/clarisse.png -O /opt/isotropix/clarisse.png
    tar -axvf /tmp/isotropix_clarisse_ifx_4.0_sp8_linux64.tar.gz
    sudo unlink /usr/share/applications/clarisse.desktop
    sudo ln -s /opt/isotropix/clarisse.desktop /usr/share/applications/clarisse.desktop
  )
  if [[ "$?" != "0" ]]; then
    sudo rm -rf /opt/isotropix
    echo "Install failed"
    exit 2
  fi

  echo "Do you have a license ? (y/N)"
  read yn
  case $yn in
    [yY] | [yY][Ee][Ss] )
      echo "You can now register your license"
      ;;
    [nN] | [n|N][O|o] | * )
      wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Clarisse_iFX_v4.0_SP8/isotropix_clarisse_ifx_4.0_sp8_hack.zip
      unzip -o isotropix_clarisse_ifx_4.0_sp8_hack.zip
      if [[ -d "hack" ]]; then
        mv /opt/isotropix/clarisse/clarisse.bin /opt/isotropix/clarisse/clarisse.bin.old
        mv /opt/isotropix/clarisse/cnode.bin /opt/isotropix/clarisse/cnode.bin.old
        cp hack/c*.bin /opt/isotropix/clarisse/
        chmod 775 /opt/isotropix/clarisse/{clarisse,cnode}.bin
      fi
      ;;
  esac

  echo "You can now run Clarisse \\o/"
fi
