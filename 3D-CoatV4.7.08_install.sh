#!/usr/bin/env bash

if [[ $(grep "Fedora 32" /etc/os-release) ]]; then
  echo "Download 3D-Coat 4.7.08b installer"
  wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/3D-CoatV4.7.08/3D-CoatV4.7.08.tar.gz -O /tmp/3D-CoatV4.7.08.tar.gz
  if [[ ! "$?" == 0 ]]; then
    echo "Install failed, nothing to install"
    exit 2
  fi

  (
    if [[ -e "/opt/Pilgway/3D-CoatV4-7" ]]; then
      sudo rm -rf /opt/Pilgway/3D-CoatV4-7
    fi

    echo "Install missings components"
    sudo dnf install -y libpng12

    echo "Install 3D-Coat"
    sudo mkdir -p /opt/Pilgway
    sudo chown sfo:root /opt/Pilgway
    cd /opt/Pilgway

    echo -e "#!/usr/bin/env bash\n/opt/Pilgway/3D-CoatV4-7/3d-coat-64" > /opt/Pilgway/startup
    chmod 755 /opt/Pilgway/startup

    cat <<EOF > /opt/Pilgway/3D-CoatV4-7.desktop
[Desktop Entry]
Name=3D-Coat 4.7.08b
Type=Application
Categories=Application;Graphics;
Icon=/opt/Pilgway/3D-CoatV4-7/3D-Coat.ico
Exec=/opt/Pilgway/startup
Encoding=UTF-8
EOF

    tar -axvf /tmp/3D-CoatV4.7.08.tar.gz

    sudo unlink /usr/share/applications/3D-CoatV4-7.desktop
    sudo ln -s /opt/Pilgway/3D-CoatV4-7.desktop /usr/share/applications/3D-CoatV4-7.desktop
  )
  if [[ "$?" != "0" ]]; then
    sudo rm -rf /opt/Pilgway
    echo "Install failed"
    exit 2
  fi

  echo "You can now run 3D-Coat \\o/"
fi
