#!/usr/bin/env bash

if [[ $(grep "Fedora 32" /etc/os-release) ]]; then
  (
  wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Fusion_16.2.1/Blackmagic_Fusion_Studio_Linux_16.2.1_installer.run -O /tmp/Blackmagic_Fusion_Studio_Linux_16.2.1_installer.run
  if [[ ! "$?" == 0 ]]; then
    echo "Install failed, nothing to install"
    exit 2
  fi
  wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Fusion_16.2.1/Blackmagic_Fusion_Render_Node_Linux_16.2.1_installer.run -O /tmp/Blackmagic_Fusion_Render_Node_Linux_16.2.1_installer.run
  if [[ ! "$?" == 0 ]]; then
    echo "Install failed, nothing to install"
    exit 2
  fi

  (
    if [[ -e "/opt/BlackmagicDesign/Fusion16" ]]; then
      echo "Please, unintall Fusion first"
      sleep 3
      sudo /opt/BlackmagicDesign/Fusion16/FusionInstaller
      sudo rm -rf /opt/BlackmagicDesign
    fi

    echo "Choose what you want to install? (type a number)"
    echo -e "\t [0] Fusion Studio*"
    echo -e "\t [1] Fusion Render Node only"
    echo -e "\t [2] Both Fusion Studio & Render Node"
    echo -e "\t [3] Nothing"

    printf "(Default: Fusion Studio) "
    read ans

    case $ans in
      3 )
        exit 1
        ;;
      2 )
        echo "Install Fusion Studio & Fusion Render Node"
        chmod 755 /tmp/Blackmagic_Fusion_Studio_Linux_16.2.1_installer.run
        chmod 755 /tmp/Blackmagic_Fusion_Render_Node_Linux_16.2.1_installer.run
        sudo /tmp/Blackmagic_Fusion_Studio_Linux_16.2.1_installer.run
        sudo /tmp/Blackmagic_Fusion_Render_Node_Linux_16.2.1_installer.run
        echo 2 > /tmp/install.log
        ;;
      1 )
        echo "Install Fusion Render Node"
        chmod 755 /tmp/Blackmagic_Fusion_Render_Node_Linux_16.2.1_installer.run
        sudo /tmp/Blackmagic_Fusion_Render_Node_Linux_16.2.1_installer.run
        echo 1 > /tmp/install.log
        ;;
      0 | * )
        echo "Install Fusion Studio"
        chmod 755 /tmp/Blackmagic_Fusion_Studio_Linux_16.2.1_installer.run
        sudo /tmp/Blackmagic_Fusion_Studio_Linux_16.2.1_installer.run
        echo 0 > /tmp/install.log
        ;;
    esac
  )
  if [[ "$?" != "0" ]]; then
    echo "Install failed"
    exit 2
  fi

  printf "Do you have a license ? (y/N*) "
  read yn
  case $yn in
    [yY] | [yY][Ee][Ss] )
      echo "You can now register your license"
      ;;
    [nN] | [n|N][O|o] | * )
      install_type=$(cat /tmp/install.log)
      case $install_type in
        2 )
          wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Fusion_16.2.1/Fusion_Studio_16.2.1_Patch.zip
          unzip -o Fusion_Studio_16.2.1_Patch.zip
          if [[ -f "libfusionsystem.so.patch" ]]; then
            cp /opt/BlackmagicDesign/Fusion16/libfusionsystem.so /opt/BlackmagicDesign/Fusion16/libfusionsystem.so.old
            sudo patch < libfusionsystem.so.patch /opt/BlackmagicDesign/Fusion16/libfusionsystem.so
          fi
          wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Fusion_Studio_16.2.1/Fusion_16.2.1_RenderNode_Patch.zip
          unzip -o Fusion_16.2.1_RenderNode_Patch.zip
          if [[ -f "libfusionsystem.so.patch" ]]; then
            #cp
            #sudo patch < libfusionsystem.so.patch
          fi
          ;;
        1 )
          wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Fusion_16.2.1/Fusion_16.2.1_RenderNode_Patch.zip
          unzip -o Fusion_16.2.1_RenderNode_Patch.zip
          if [[ -f "libfusionsystem.so.patch" ]]; then
            #cp
            #sudo patch < libfusionsystem.so.patch
          fi
          ;;
        0 )
          wget -c https://gitlab.com/sfeuga/cg-apps/-/raw/Fusion_16.2.1/Fusion_Studio_16.2.1_Patch.zip
          unzip -o Fusion_Studio_16.2.1_Patch.zip
          if [[ -f "libfusionsystem.so.patch" ]]; then
            cp /opt/BlackmagicDesign/Fusion16/libfusionsystem.so /opt/BlackmagicDesign/Fusion16/libfusionsystem.so.old
            sudo patch < libfusionsystem.so.patch /opt/BlackmagicDesign/Fusion16/libfusionsystem.so
          fi
          ;;
      esac
      ;;
  esac

  echo "You can now run Fusion Studio and/or use Fusion Render Node \\o/"
fi
